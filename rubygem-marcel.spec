%global gem_name marcel
Name:                rubygem-%{gem_name}
Version:             1.0.2
Release:             2
Summary:             Simple mime type detection using magic numbers, file names, and extensions
License:             MIT and ASL 2.0
URL:                 https://github.com/basecamp/marcel
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
# git clone https://github.com/rails/marcel.git && cd marcel
# git archive -v -o marcel-1.0.2-test.tar.gz v1.0.2 test/
Source1:             %{gem_name}-%{version}-test.tar.gz
# https://github.com/rails/marcel/commit/12fc8daae656ccb09441c4a3c376b5de3af05172
Patch0:              marcel-minitest-5_19-compat.patch
BuildRequires:       ruby(release) rubygems-devel ruby >= 2.2 rubygem(minitest)
BuildRequires:       rubygem(rack)
BuildArch:           noarch
%description
Simple mime type detection using magic numbers, file names, and extensions.

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -a 1
%patch -P0 -p1

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
ln -sf $(pwd)/test .%{gem_instdir}
pushd .%{gem_instdir}
sed -i "/require 'byebug'/ s/^/#/" test/test_helper.rb
ruby -Ilib:test -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%license %{gem_instdir}/APACHE-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.md

%changelog
* Thu Feb 27 2025 jiangxinyu <jiangxinyu@kylinos.cn> - 1.0.2-2
- Fix JRuby build error

* Thu Mar 03 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 1.0.2-1
- update to 1.0.2

* Wed Aug 19 2020 geyanan <geyanan2@huawei.com> - 0.3.2-1
- package init
